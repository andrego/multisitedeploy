﻿using System;
using NLog;
using Utility;

namespace Deploy
{
    public class DeployManager : IDeployManager
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }

        private readonly Logger _logger;


        public DeployManager(string username, string password, string domain, Logger logger = null)
        {
            UserName = username;
            Password = password;
            Domain = domain;
            _logger = logger ?? LogManager.GetCurrentClassLogger();

        }

        public void DeployClient(string destination, string source, string processName, string host)
        {
            var fileManager = new NetworkFileManager(Domain, UserName, Password);
            var processManager = new NetworkProcessManager(host, Domain, UserName, Password);

            try
            {
                _logger.Debug(String.Format("Try to DeployClient host:{0}, domain:{1}, user:{2}, password:{3}", host,
                    Domain, UserName, Password));
                _logger.Debug(String.Format("[{0}] Try to stop process: {1}", host, processName));
                processManager.StopProcess(processName);
                _logger.Debug(String.Format("[{0}] Stop process success", host));

                _logger.Debug(String.Format("[{0}] Try to copy directory from [{1}] to [\\{2}\\{3}]", host, source, host,
                    destination));
                fileManager.CopyDirectory(String.Format("\\\\{0}\\{1}", host, destination), source, true);
                _logger.Debug(String.Format("[{0}] Copy directory success", host));
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("DeployClient: {0}", ex.Message));
                throw;
            }
        }

        public void DeployService(string destination, string source, string serviceName, string host)
        {
            var fileManager = new NetworkFileManager(Domain, UserName, Password);
            var serviceManager = new NetworkServiceManager(host, Domain, UserName, Password);

            try
            {
                _logger.Debug(String.Format("Try to DeployService host:{0}, domain:{1}, user:{2}, password:{3}", host,
                    Domain, UserName, Password));
                _logger.Debug(String.Format("[{0}] Try to stop service: {1}", host, serviceName));
                serviceManager.StopService(serviceName, 3);
                _logger.Debug(String.Format("[{0}] Stop service success", host));

                _logger.Debug(String.Format("[{0}] Try to copy directory from {1} to {2}", host, source, destination));
                fileManager.CopyDirectory(String.Format("\\\\{0}\\{1}", host, destination), source, true);
                _logger.Debug(String.Format("[{0}] Copy directory success", host));

                _logger.Debug(String.Format("[{0}] Try to start service: {1}", host, serviceName));
                serviceManager.StartService(serviceName, 3);
                _logger.Debug(String.Format("[{0}] Start service success", host));
            }
            catch (Exception ex)
            {
                _logger.Error(String.Format("DeployService: {0}", ex.Message));
                throw;
            }
        }

        public void CheckService(string serviceName, string host, Logger checkLogger)
        {
            var serviceManager = new NetworkServiceManager(host, Domain, UserName, Password);
            _logger.Debug(String.Format(
                "Try to CheckService host:{0}, domain:{1}, user:{2}, password:{3}, service:{4}", host, Domain, UserName,
                Password, serviceName));
            serviceManager.GetServiceInfo(serviceName, checkLogger);
        }
    }
}
