﻿using NLog;

namespace Deploy
{
    public interface IDeployManager
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Domain { get; set; }


        void DeployClient(string destination, string source, string processName, string host);
        void DeployService(string destination, string source, string serviceName, string host);
        void CheckService(string serviceName, string host, Logger checkLooger);
    }
}
