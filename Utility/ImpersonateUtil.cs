﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Utility
{
    /// <summary>
    /// Impersonate a windows logon.
    /// </summary>
    public class ImpersonationUtil
    {
        private static bool IsImpersonated { get; set; }

        /// <summary>
		/// Impersonate given logon information.
		/// </summary>
		/// <param name="logon">Windows logon name.</param>
		/// <param name="password">password</param>
		/// <param name="domain">domain name</param>
		/// <returns></returns>
		public static bool Impersonate(string logon, string password, string domain)
        {
            if (IsImpersonated) return true;

            IntPtr token = IntPtr.Zero;
            IntPtr tokenDuplicate = IntPtr.Zero;

            if (LogonUser(logon, domain, password, 9,
                    0, ref token) != 0)
            {

                if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                {
                    var tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                    _impersonationContext = tempWindowsIdentity.Impersonate();
                    if (null != _impersonationContext)
                    {
                        IsImpersonated = true;
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Unimpersonate.
        /// </summary>
        public static void UnImpersonate()
        {
            _impersonationContext.Undo();
            IsImpersonated = false;
        }

        [DllImport("advapi32.DLL", SetLastError = true)]
        public static extern int LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType,
                    int dwLogonProvider, ref IntPtr phToken);

        [DllImport("advapi32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(
            IntPtr hToken,
            int impersonationLevel,
            ref IntPtr hNewToken);

        private const int LOGON32_LOGON_INTERACTIVE = 2;
        private const int LOGON32_LOGON_NETWORK_CLEARTEXT = 4;
        private const int LOGON32_PROVIDER_DEFAULT = 0;
        private static WindowsImpersonationContext _impersonationContext;
    }
}
