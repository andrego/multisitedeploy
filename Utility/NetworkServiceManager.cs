﻿using System;
using System.ServiceProcess;
using NLog;

namespace Utility
{
    public class NetworkServiceManager
    {
        private readonly string _userName;
        private readonly string _password;
        private readonly string _domain;
        private readonly string _host;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NetworkServiceManager(string host, string domain, string userName, string password)
        {
            _domain = domain;
            _userName = userName;
            _password = password;
            _host = host;
            
        }

        /// <summary>
        /// Print detailed information on a particular service.
        /// </summary>
        /// <param name="name">service name.</param>
        /// <param name="logger">NLog Logger instance to use.</param>
        public void GetServiceInfo(string name, Logger logger=null)
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                var serviceControllerservice = new ServiceController(name, _host);

                string serviceName = serviceControllerservice.ServiceName;
                string displayName = serviceControllerservice.DisplayName;
                ServiceControllerStatus status = serviceControllerservice.Status;

                // print out detail service information
                //Logger.Info("Service Name                 : {0}", serviceName);
                //Logger.Info("Display Name                 : {0}", displayName);
                //Logger.Info("Service Status               : {0}", status.ToString());
                //Logger.Info("Service Type                 : {0}", serviceControllerservice.ServiceType.ToString());
                //Logger.Info("Service Can Stop             : {0}", serviceControllerservice.CanStop);
                //Logger.Info("Service Can Pause / Continue : {0}", serviceControllerservice.CanPauseAndContinue);
                //Logger.Info("Service Can Shutdown         : {0}", serviceControllerservice.CanShutdown);

                if (logger != null)
                {
                    logger.Info(String.Format("[{0}] {1} {2}", _host, serviceName, status));
                }

                // print out dependent services
                ServiceController[] dependedServices = serviceControllerservice.DependentServices;
                //Logger.Info("{0} Depended Service(s)        : ", dependedServices.Length.ToString());

                int pos = 0;
                foreach (ServiceController dService in dependedServices)
                {
                    Logger.Info("{0}{1}", ((dependedServices.Length > 1 && pos > 0) ? ", " : string.Empty), dService.ServiceName);
                    pos++;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetService failed: {0}", ex.Message);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }
        }

        /// <summary>
        /// Get all available services on a computer.
        /// </summary>
        public void GetServices()
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                ServiceController[] services = ServiceController.GetServices(_host);
                foreach (ServiceController service in services)
                {
                    Logger.Info("{0} [ {1} ]", service.ServiceName, service.Status.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetServices failed: {0}", ex.Message);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }
        }

        /// <summary>
        /// Restart a service.
        /// This action will in turn call StopService and StartService.
        /// If the service is not currently stopped, it will try to stop the service first.
        /// </summary>
        /// <param name="name">service name.</param>
        /// <param name="timeout">timeout value used for stopping and restarting.</param>
        public void RestartService(string name, int timeout)
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                var serviceControllerservice = new ServiceController(name, _host);

                if (ServiceControllerStatus.Stopped != serviceControllerservice.Status)
                {
                    StopService(name, timeout);
                }

                StartService(name, timeout);
            }
            catch (Exception ex)
            {
                Logger.Error("RestartService failed: {0}", ex.Message);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }
        }

        /// <summary>
        /// Start a service.
        /// </summary>
        /// <param name="name">service name.</param>
        /// <param name="timeout">timeout value for starting in seconds.</param>
        public void StartService(string name, int timeout)
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                var serviceControllerservice = new ServiceController(name, _host);
                if (ServiceControllerStatus.Stopped == serviceControllerservice.Status)
                {
                    Logger.Debug("Starting service '{0}' on '{1}' ...",
                        serviceControllerservice.ServiceName, serviceControllerservice.MachineName);

                    serviceControllerservice.Start();

                    if (int.MinValue != timeout)
                    {
                        TimeSpan t = TimeSpan.FromSeconds(timeout);
                        serviceControllerservice.WaitForStatus(ServiceControllerStatus.Running, t);

                    }
                    else serviceControllerservice.WaitForStatus(ServiceControllerStatus.Running);

                    Logger.Debug("Started service '{0}' on '{1}'",
                        serviceControllerservice.ServiceName, serviceControllerservice.MachineName);
                }
                else
                {
                    Logger.Debug("Can not start service '{0}' on '{1}'",
                        serviceControllerservice.ServiceName, serviceControllerservice.MachineName);

                    Logger.Debug("Service State '{0}'", serviceControllerservice.Status.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("StartService failed: {0}", ex.Message);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }

        }

        /// <summary>
        /// Stop a service.
        /// </summary>
        /// <param name="name">service name.</param>
        /// <param name="timeout">timeout for stopping the service in seconds.</param>
        public void StopService(string name, int timeout)
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                var serviceControllerservice = new ServiceController(name, _host);

                if (serviceControllerservice.CanStop)
                {
                    Logger.Debug("Stopping service '{0}' on '{1}' ...",
                        serviceControllerservice.ServiceName, serviceControllerservice.MachineName);

                    serviceControllerservice.Stop();

                    if (int.MinValue != timeout)
                    {
                        TimeSpan t = TimeSpan.FromSeconds(timeout);
                        serviceControllerservice.WaitForStatus(ServiceControllerStatus.Stopped, t);

                    }
                    else serviceControllerservice.WaitForStatus(ServiceControllerStatus.Stopped);

                    Logger.Debug("Stopped service '{0}' on '{1}'\r\n",
                        serviceControllerservice.ServiceName, serviceControllerservice.MachineName);

                }
                else
                {
                    Logger.Debug("Can not stop service '{0}' on '{1}'",
                        serviceControllerservice.ServiceName, serviceControllerservice.MachineName);

                    Logger.Debug("Service State '{0}'", serviceControllerservice.Status.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("StopService failed: {0}", ex.Message);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }
        }
    }
}
