﻿using System;
using System.Data;
using System.IO;
using NLog;

namespace Utility
{
    public class NetworkFileManager
    {
        private readonly string _userName;
        private readonly string _password;
        private readonly string _domain;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NetworkFileManager(string domain, string userName, string password)
        {
            _domain = domain;
            _userName = userName;
            _password = password;
        }

        public void CopyFile(string destination, string source)
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                File.Copy(destination, source, true);
            }
            catch (Exception ex)
            {
                Logger.Error("Copying failed: {0}", ex.Message);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }
        }

        public void CopyDirectory(string destination, string source, bool copySubDirs)
        {
            if (!ImpersonationUtil.Impersonate(_userName, _password, _domain))
            {
                Logger.Error("No such account found, Impersonation failed.");
                return;
            }
            try
            {
                LocalCopyDirectory(destination, source, copySubDirs);
            }
            catch (Exception ex)
            {
                Logger.Error("{0} {1}", ex.Message, ex.StackTrace);
                throw;
            }
            finally
            {
                ImpersonationUtil.UnImpersonate();
            }
        }

        private void LocalCopyDirectory(string destination, string source, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(source);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + source);
            }


            if (!Directory.Exists(destination))
            {
                Logger.Debug("CreateDirectory: {0}", destination);
                Directory.CreateDirectory(destination);
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destination, file.Name);
                Logger.Debug("Copying file: {0} to {1}", file.Name, temppath);
                file.CopyTo(temppath, true);
            }

            if (copySubDirs)
            {
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destination, subdir.Name);
                    Logger.Debug("CopyDirectory: {0} to {1}", subdir.FullName, temppath);
                    LocalCopyDirectory(temppath, subdir.FullName, true);
                }
            }
        }
    }
}
