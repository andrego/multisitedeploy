﻿using System;
using System.Management;
using NLog;

namespace Utility
{
    public class NetworkProcessManager
    {
        private readonly string _userName;
        private readonly string _password;
        private readonly string _domain;
        private readonly string _host;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NetworkProcessManager(string host, string domain, string userName, string password)
        {
            _domain = domain;
            _userName = userName;
            _password = password;
            _host = host;
        }

        /// <summary>
        /// Get all available processes on a computer.
        /// </summary>
        public void GetProcesses()
        {
            var connectoptions = new ConnectionOptions
            {
                Username = String.Format("{0}\\{1}", _domain, _userName),
                Password = _password
            };
            try
            {
                ManagementScope scope = new ManagementScope(@"\\" + _host + @"\root\cimv2", connectoptions);
                // WMI query
                var query = new SelectQuery("select * from Win32_process");

                using (var searcher = new ManagementObjectSearcher(scope, query))
                {
                    var processOwner = "";
                    var processDomain = "";
                    foreach (ManagementObject process in searcher.Get())
                    {
                        string[] o = new String[2];

                        process.InvokeMethod("GetOwner", (object[]) o);
                        processOwner = o[0];
                        if (processOwner == null)
                            processOwner = String.Empty;
                        processDomain = o[1];
                        if (processDomain == null)
                            processDomain = String.Empty;
                        Logger.Info("Name : {0}, Owner : {1}, Domain : {2}", process["Name"], processOwner,
                            processDomain);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetProcesses failed: {0}", ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Stop selected processes on a computer.
        /// </summary>
        public void StopProcess(string processName)
        {
            var connectoptions = new ConnectionOptions
            {
                Username = String.Format("{0}\\{1}", _domain, _userName),
                Password = _password
            };
            try
            {
                ManagementScope scope = new ManagementScope(@"\\" + _host + @"\root\cimv2", connectoptions);
                // WMI query
                var query = new SelectQuery(String.Format("select * from Win32_process where Name = '{0}'", processName));

                using (var searcher = new ManagementObjectSearcher(scope, query))
                {
                    var processOwner = "";
                    var processDomain = "";
                    foreach (ManagementObject process in searcher.Get())
                    {
                        process.InvokeMethod("Terminate", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("StopProcesses failed: {0}", ex.Message);
                throw;
            }
        }
    }
}
