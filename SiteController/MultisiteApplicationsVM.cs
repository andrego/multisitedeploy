﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SiteController
{
    [DataContract]
    public class MultisiteApplicationsVM
    {
        [DataMember]
        [JsonProperty(PropertyName = "ID")]
        public int ID { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "IPAddress")]
        public string IPAddress { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "MachineName")]
        public string MachineName { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "ApplicationType")]
        public string ApplicationType { get; set; }
        [DataMember]
        [JsonProperty(PropertyName = "LastUpdate")]
        public DateTime LastUpdate { get; set; }

        public bool Checked { get; set; }
    }


}
