﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using System.Linq;
using Deploy;
using NLog;

namespace SiteController
{
    class VM : ViewModelBase, INotifyPropertyChanged
    {
        private static Logger logger = LogManager.GetLogger("DeployControllerLogger");
        private static Logger checkLogger = LogManager.GetLogger("CheckLogger");

        private Thread _appThread;
        private IDeployManager deployManager;
        private string _userName;
        private string _password;
        private string _domain;
        private string _serviceName;
        private string _processName;
        private string _clientSourceDirectory;
        private string _serviceSourceDirectory;
        private string _clientDestinationDirectory;
        private string _serviceDestinationDirectory;

        public VM()
        {
            BindHosts();
            IsInProgress = Visibility.Hidden;
            ProgressValue = 0;

            try
            {
                ReadSettings();
            }
            catch (Exception ex)
            {
                logger.Error(String.Format("ReadConfigError: {0}", ex.Message));
                throw;
            }

            deployManager = new DeployManager(_userName, _password, _domain, logger);

        }

        private enum Types
        {
            Client = 1, Server = 2
        }

        private void ReadSettings()
        {
            _userName = ConfigurationSettings.AppSettings["UserName"];
            _password = ConfigurationSettings.AppSettings["Password"];
            _domain = ConfigurationSettings.AppSettings["Domain"];
            _serviceName = ConfigurationSettings.AppSettings["ServiceName"];
            _processName = ConfigurationSettings.AppSettings["ProcessName"];
            _clientSourceDirectory = ConfigurationSettings.AppSettings["ClientSourceDirectory"];
            _clientDestinationDirectory = ConfigurationSettings.AppSettings["ClientDestinationDirectory"];
            _serviceSourceDirectory = ConfigurationSettings.AppSettings["ServiceSourceDirectory"];
            _serviceDestinationDirectory = ConfigurationSettings.AppSettings["ServiceDestinationDirectory"];
        }


        private void BindHosts()
        {
            Apps = new ObservableCollection<MultisiteApplicationsVM>();
            GetApplications();
        }
        private ObservableCollection<MultisiteApplicationsVM> _apps;
        public ObservableCollection<MultisiteApplicationsVM> Apps
        {
            get { return _apps; }
            set { _apps = value; }
        }
        private RelayCommand _runCommamd;

        public RelayCommand RunCommamd
        {
            get
            {
                if (_runCommamd == null)
                {
                    _runCommamd = new RelayCommand(RunCommamdExecute);
                }
                return _runCommamd;
            }
            set
            {
                _runCommamd = value;
            }
        }
        private RelayCommand _cancelCommamd;
        public RelayCommand CancelCommamd
        {
            get
            {
                if (_cancelCommamd == null)
                {
                    _cancelCommamd = new RelayCommand(CancelCommamdExecute);
                }
                return _cancelCommamd;
            }
            set
            {
                _cancelCommamd = value;
            }
        }

        private void CancelCommamdExecute()
        {
            if (_appThread !=null && _appThread.IsAlive)
                _appThread.Abort();
            ProgressValue = 0;
            IsInProgress = Visibility.Hidden;
        }
        private RelayCommand _checkCommamd;
        public RelayCommand CheckCommamd
        {
            get
            {
                if (_checkCommamd == null)
                {
                    _checkCommamd = new RelayCommand(CheckCommamdExecute);
                }
                return _checkCommamd;
            }
            set
            {
                _cancelCommamd = value;
            }
        }

        private void CheckCommamdExecute()
        {
            if(Apps.Any(a => a.Checked))
            {
                _appThread = new Thread(Check);
                _appThread.Start();
            }
            else
            {
                MessageBox.Show("Please check some hosts");
            }
           
        }


        private void RunCommamdExecute()
        {
            if (Apps.Any(a => a.Checked))
            {
                _appThread = new Thread(Run);
                _appThread.Start();
            }
            else
            {
                MessageBox.Show("Please check some hosts");
            }
        }

        private void Run()
        {
            if (IsInProgress == Visibility.Hidden)
            {
                IsInProgress = Visibility.Visible;
                ProgressValue = 0;
                var step = (double)100 / Apps.Count(a => a.Checked);
                foreach (var app in Apps.Where(a => a.Checked))
                {
                    try
                    {
                        logger.Info(String.Format("Deploy {0} type {1}", app.IPAddress, app.ApplicationType));
                        switch (app.ApplicationType)
                        {

                            case "Client":
                                deployManager.DeployClient(_clientDestinationDirectory, _clientSourceDirectory, _processName,
                                    app.IPAddress);
                                break;
                            case "Server":
                                deployManager.DeployService(_serviceDestinationDirectory, _serviceSourceDirectory,
                                    _serviceName, app.IPAddress);
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(String.Format("Error on {0}. Stopped", app.IPAddress));
                        IsInProgress = Visibility.Hidden;
                        return;
                    }
                    ProgressValue += step;
                }
                IsInProgress = Visibility.Hidden;
            }
        }

        private void Check()
        {
            if (IsInProgress == Visibility.Hidden)
            {
                IsInProgress = Visibility.Visible;
                ProgressValue = 0;
                var step = (double)100 / Apps.Count(a => a.Checked && a.ApplicationType == "Server");
                foreach (var app in Apps.Where(a => a.Checked && a.ApplicationType == "Server"))
                {
                    logger.Info(String.Format("Check {0} type {1}", app.IPAddress, app.ApplicationType));
                    deployManager.CheckService(_serviceName, app.IPAddress, checkLogger);
                    ProgressValue += step;
                }
                IsInProgress = Visibility.Hidden;
            }
        }

        private Visibility _isInProgress;

        public Visibility IsInProgress
        {
            get { return _isInProgress; }
            set
            {
                _isInProgress = value;
                RaisePropertyChanged("IsInProgress");
            }
        }

        public double _progressValue;
        public double ProgressValue
        {
            get { return _progressValue; }
            set
            {
                double result;
                if (double.TryParse(String.Format("{0:#.##}", value), out result))
                    _progressValue = result;
                else
                    _progressValue = (int)value;
                RaisePropertyChanged("ProgressValue");
            }

        }

        private async void GetApplications()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var res = await client.GetAsync("http://80.179.206.121:62023/DataServices/api/ApplicationsInstalled");

                if (res.IsSuccessStatusCode)
                {
                    var resString = await res.Content.ReadAsStringAsync();

                    var apps = JsonConvert.DeserializeObject<List<MultisiteApplicationsVM>>(resString);
                    apps.ForEach(x =>
                    {
                        if (!_apps.Contains(x))
                        {
                            x.ApplicationType = Enum.GetName(typeof (Types), (int.Parse(x.ApplicationType)));
                            x.Checked = false;
                            _apps.Add(x);
                        }
                    });
                }
                else
                {
                    MessageBox.Show("Error. Check internet connection");
                }
            }

            //    for (int i = 0; i < 3; i++)
            //{
            //    var obj = new MultisiteApplicationsVM
            //    {
            //        ApplicationType = "Client",
            //        Checked = false,
            //        IPAddress = "192.168.174.130",
            //        MachineName = "192.168.174.130",
            //        LastUpdate = DateTime.Now.AddDays(-2)
            //    };
            //    _apps.Add(obj);
            //}
            //for (int i = 0; i < 30; i++)
            //{
            //    var obj = new MultisiteApplicationsVM
            //    {
            //        ApplicationType = "Server",
            //        Checked = false,
            //        IPAddress = "192.168.174.130",
            //        MachineName = "192.168.174.130",
            //        LastUpdate = DateTime.Now.AddDays(-2)
            //    };
            //    _apps.Add(obj);
            //}
        }
    }
}
